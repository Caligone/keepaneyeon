<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Generate a new password, set it and return it
	 * @return string
	 */
	public function generatePassword()
	{
		$newPassword = substr(str_replace('/', 't', Hash::make(rand().'42')), 10, 20);
		$this->password = Hash::make($newPassword);
		return $newPassword;
	}

	/**
	 * Generate a new token, set it and return it
	 * @return string
	 */
	public function generateToken()
	{
		$this->token = str_replace('/', 't', Hash::make(rand().'42'));
		return $this->token;
	}

    
    public function orders()
    {
        return $this->hasMany('Order');
    }
    
    public function giftcodes()
    {
        return $this->hasMany('Giftcode');
    }
    
    public function eyes()
    {
        return $this->belongsToMany('Eye');
    }
}
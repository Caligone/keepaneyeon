<!DOCTYPE html>
<html>
<head>
<title>KeepAnEyeOn</title>
<meta charset="utf-8">

<link rel="icon" type="image/png" href="/assets/img/favicon.png"/>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

{{ HTML::style('assets/css/bootstrap.min.css') }}
{{ HTML::style('assets/css/font-awesome.min.css') }}
{{ HTML::style('assets/css/custom.css') }}

{{ HTML::script('assets/js/jquery-1.10.2.min.js') }}
{{ HTML::script('assets/js/bootstrap.min.js') }}

</head>
<body>
    @include('menu_template')
    @include('alert_template')
	<div class="clearfix"></div>

	<div class="container">
	    @yield('content')
	</div>
	<div class="clearfix more-space"></div>

    @yield('scripts')
</body>
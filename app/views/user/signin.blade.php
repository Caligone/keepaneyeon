@extends('main_template')

@section('content')
<div class="jumbotron">
    <h1><i class="icon-eye-open" style="font-size:3em;"></i></h1>
    <p class="lead">{{ Lang::get('keepaneyeon.signin') }}</p>
    {{ Form::open(array('url' => 'signin', 'class' => 'col-lg-6 col-lg-offset-3')) }}
    <fieldset>
        <div class="form-group" style="margin:5px">
            {{ Form::label('username', Lang::get('keepaneyeon.username')) }}
            {{ Form::text('username', null, array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-username'))) }}
        </div>
        <div class="form-group" style="margin:5px">
            {{ Form::label('password', Lang::get('keepaneyeon.password')) }}
            {{ Form::password('password', array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-password'))) }}
        </div>
    {{ Form::submit(Lang::get('keepaneyeon.signin'), array("class" => "col-lg-6 pull-left btn btn-default ")) }}
    {{ Form::reset(Lang::get('keepaneyeon.reset'), array("class" => "col-lg-6 pull-right btn btn-default")) }}
    {{ Form::close() }}
    </fieldset>
</div>
@stop
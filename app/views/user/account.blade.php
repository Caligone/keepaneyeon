@extends('main_template')

@section('content')
<div class="jumbotron">
    <h1><i class="icon-eye-open" style="font-size:3em;"></i></h1>
    <p class="lead">{{ Lang::get('keepaneyeon.account-lead', array('username' => $user->username)) }}</p>
        {{ Form::open(array('url' => 'account', 'class' => 'col-lg-6 col-lg-offset-3')) }}
            @if($errors->has('password'))
                <span class="label label-danger">{{{ $errors->first('password') }}}</span>
            @endif
            @if($errors->has('new-password'))
                <span class="label label-danger">{{{ $errors->first('new-password') }}}</span>
            @endif
            @if($errors->has('confirm-new-password'))
                <span class="label label-danger">{{{ $errors->first('confirm-new-password') }}}</span>
            @endif
            @if($errors->has('email'))
                <span class="label label-danger">{{{ $errors->first('email') }}}</span>
            @endif
            @if($errors->has('confirm-email'))
                <span class="label label-danger">{{{ $errors->first('confirm-email') }}}</span>
            @endif
    <fieldset>
        <div class="form-group" style="margin:5px">
            {{ Form::label('password', Lang::get('keepaneyeon.old-password')) }}
            {{ Form::password('password', array("class" => "form-control ", "placeholder" =>Lang::get('keepaneyeon.ph-old-password'))) }}
            {{ Form::label('new-password', Lang::get('keepaneyeon.new-password')) }}
            {{ Form::password('new-password', array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-new-password'))) }}
            {{ Form::label('confirm-new-password', Lang::get('keepaneyeon.c-new-password')) }}
            {{ Form::password('confirm-new-password', array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-c-new-password'))) }}
        </div>
        <div class="form-group" style="margin:5px">
            {{ Form::label('email', Lang::get('keepaneyeon.email')) }}
            {{ Form::email('email', null, array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-email'))) }}
            {{ Form::label('confirm-email', Lang::get('keepaneyeon.c-email')) }}
            {{ Form::email('confirm-email', null, array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-c-email'))) }}
        </div>
    {{ Form::submit(Lang::get('keepaneyeon.edit'), array("class" => "col-lg-6 pull-left btn btn-default ")) }}
    {{ Form::reset(Lang::get('keepaneyeon.reset'), array("class" => "col-lg-6 pull-right btn btn-default")) }}
    {{ Form::close() }}
    </fieldset>
</div>
@stop
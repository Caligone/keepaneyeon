@extends('main_template')

@section('content')
<div class="jumbotron">
    <h1><i class="icon-eye-open" style="font-size:3em;"></i></h1>
    <p class="lead">{{ Lang::get('keepaneyeon.signup-lead') }}</p>
    {{ Form::open(array('url' => 'signup', 'class' => 'col-lg-6 col-lg-offset-3')) }}
            @if($errors->has('username'))
                <span class="label label-danger">{{{ $errors->first('username') }}}</span>
            @endif
            @if($errors->has('password'))
                <span class="label label-danger">{{{ $errors->first('password') }}}</span>
            @endif
            @if($errors->has('confirm-password'))
                <span class="label label-danger">{{{ $errors->first('confirm-password') }}}</span>
            @endif
            @if($errors->has('email'))
                <span class="label label-danger">{{{ $errors->first('email') }}}</span>
            @endif
            @if($errors->has('confirm-email'))
                <span class="label label-danger">{{{ $errors->first('confirm-email') }}}</span>
            @endif
    <fieldset>
        <div class="form-group" style="margin:5px">
            {{ Form::label('username', Lang::get('keepaneyeon.username')) }}
            {{ Form::text('username', null, array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-username'))) }}
        </div>
        <div class="form-group" style="margin:5px">
            {{ Form::label('password', Lang::get('keepaneyeon.password')) }}
            {{ Form::password('password', array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-password'))) }}
            {{ Form::label('confirm-password', Lang::get('keepaneyeon.c-password')) }}
            {{ Form::password('confirm-password', array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-c-password'))) }}
        </div>
        <div class="form-group" style="margin:5px">
            {{ Form::label('email', Lang::get('keepaneyeon.email')) }}
            {{ Form::email('email', null, array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-email'))) }}
            {{ Form::label('confirm-email', Lang::get('keepaneyeon.c-email')) }}
            {{ Form::email('confirm-email', null, array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-c-email'))) }}
        </div>
    {{ Form::submit(Lang::get('keepaneyeon.signup'), array("class" => "col-lg-6 pull-left btn btn-default ", "name" => "action", "value" => "signup")) }}
    {{ Form::reset(Lang::get('keepaneyeon.reset'), array("class" => "col-lg-6 pull-right btn btn-default")) }}
    {{ Form::close() }}
    </fieldset>
</div>
@stop
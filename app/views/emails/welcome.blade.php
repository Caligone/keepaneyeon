<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Action of your eyes</title>
	{{ HTML::style('assets/css/bootstrap.min.css') }}
	{{ HTML::style('assets/css/font-awesome.min.css') }}
	{{ HTML::style('assets/css/custom.css') }}
</head>
<body>
<div class="container">
	<div class="jumbotron">
    <h1><i class="icon-eye-open" style="font-size:3em;"></i></h1>
    <p class="lead">Dear {{ $user->username }},</br>We are glad Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, assumenda animi voluptas nam doloremque iure hic! Dolorum debitis fuga repellendus! Voluptatem, nobis, dignissimos obcaecati quia fugit praesentium aliquam magni in.</p>
</div>
<div class="container">
	<div class="jumbotron">
    <p>For now, your account is inactive and you need to <a href="{{ URL::route('Activate', array('token' => $user->token)) }}">active it</a>.</p>
</div>

</body>
</html>
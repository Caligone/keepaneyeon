@if(Session::has('danger'))
    <div class="alert alert-danger col-lg-6 col-lg-offset-3"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('danger') }}</div>
@endif

@if(Session::has('info'))
    <div class="alert alert-info col-lg-6 col-lg-offset-3"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('info') }}</div>
@endif

@if(Session::has('success'))
    <div class="alert alert-success col-lg-6 col-lg-offset-3"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('success') }}</div>
@endif
@extends('main_template')

@section('content')
<div class="jumbotron">
    <h1><i class="icon-eye-open" style="font-size:3em;"></i></h1>
    <p class="lead">{{ Lang::get('keepaneyeon.add-eye') }}</p>
    @if($errors->has('url'))
        <span class="label label-danger">{{{ $errors->first('url') }}}</span>
    @endif
    {{ Form::open(array('url' => 'eye/activate', 'class' => 'col-lg-6 col-lg-offset-3')) }}
    <fieldset>
        <div class="form-group" style="margin:5px">
            {{ Form::label('url', Lang::get('keepaneyeon.url')) }}
            {{ Form::text('url', null, array("class" => "form-control ", "placeholder" => Lang::get('keepaneyeon.ph-url'))) }}
        </div>
    {{ Form::submit(Lang::get('keepaneyeon.add'), array("class" => "col-lg-6 pull-left btn btn-default ")) }}
    {{ Form::reset(Lang::get('keepaneyeon.reset'), array("class" => "col-lg-6 pull-right btn btn-default")) }}
    {{ Form::close() }}
    </fieldset>
</div>
@stop
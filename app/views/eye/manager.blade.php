@extends('main_template')

@section('content')
<div class="jumbotron">
    <h1><i class="icon-eye-open" style="font-size:3em;"></i></h1>
    <p class="lead">{{ Lang::get('keepaneyeon.manager-lead') }}</p>
</div>
<div class="container">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>{{Lang::get('keepaneyeon.url')}}</th>
				<th>{{Lang::get('keepaneyeon.creation-date')}}</th>
				<th>{{Lang::get('keepaneyeon.update-date')}}</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($eyes as $eye)
				<tr>
					<td>{{$eye->id}}</td>
					<td><a href="{{$eye->url}}">{{$eye->url}}</a></td>
					<td>{{$eye->created_at}}</td>
					<td>{{$eye->updated_at}}</td>
					<td><a href="{{ URL::route('EditEye', array('id' => $eye->id)) }}"><i class="icon-pencil"></i></a></td>
					<td><a href="{{ URL::route('RemoveEye', array('id' => $eye->id)) }}"><i class="icon-remove"></i></a></td>
				</tr>
			@endforeach
		</tbody>
	</table>
    <p class="text-center"><a href="{{ URL::route('ActivateEye') }}"><button class="btn btn-default btn-lg">{{Lang::get('keepaneyeon.add-eye')}}</button></a></p>
</div>
@stop
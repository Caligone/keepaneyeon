<div class="navbar navbarinverse navbar-fixed-top">
    <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ URL::route('Home') }}"><i class="icon-eye-open icon-6"></i> KeepAnEyeOn</a>
        <div class="nav-collapse collapse">
            <ul class="nav navbar-nav">
                <li {{ Request::is('/') ? 'class="active"' : '' }}><a href="{{ URL::route('Home') }}">{{ Lang::get('keepaneyeon.home')}}</a></li>
                <li {{ Request::is('about') ? 'class="active"' : '' }}><a href="{{ URL::route('About') }}">{{ Lang::get('keepaneyeon.about')}}</a></li>
                <li {{ Request::is('contact') ? 'class="active"' : '' }}><a href="{{ URL::route('Contact') }}">{{ Lang::get('keepaneyeon.contact')}}</a></li>
            </ul>
        </div>
        <div class="nav-collapse collapse pull-right">
            <ul class="nav navbar-nav">
                <li class="pull-right"><a href="{{ URL::route('LangAction') }}">{{ Lang::get('keepaneyeon.icon-locale') }}</a></li>
                    @if(Auth::check())
                    <li class="dropdown pull-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{{ Auth::user()->username }}} <i class="icon-user icon-large"></i><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ URL::route('Account') }}"><i class="icon-cogs"></i> {{ Lang::get('keepaneyeon.account') }}</a></li>
                            <li><a href="{{ URL::route('EyeCredit') }}"><i class="icon-eye-open"></i> {{ Lang::get('keepaneyeon.get-eyes') }}</a></li>
                            <li><a href="{{ URL::route('ManageEyes') }}"><i class="icon-eye-open"></i> {{ Lang::get('keepaneyeon.manage-eyes') }}</a></li>
                            <li><a href="{{ URL::route('SignOutAction') }}"><i class="icon-signout"></i> {{ Lang::get('keepaneyeon.signout') }}</a></li>
                        </ul>
                    </li>
                    <li class="pull-right">
                            @if(Auth::user()->eyecredit >= 4)
                                <a href="{{ URL::route('ActivateEye') }}"><span class="label label-success">{{Auth::user()->eyecredit}} {{Lang::get('keepaneyeon.eyes')}}</span></a>
                            @elseif(Auth::user()->eyecredit >= 2)
                                <a href="{{ URL::route('EyeCredit') }}"><span class="label label-warning">{{Auth::user()->eyecredit}} {{Lang::get('keepaneyeon.eyes')}}</span></a>
                            @else
                                <a href="{{ URL::route('EyeCredit') }}"><span class="label label-danger">{{Auth::user()->eyecredit}} {{Lang::get('keepaneyeon.eye')}} </span></a>
                            @endif
                    </li>
                    @else
                    <li class="dropdown pull-right hidden-sm">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-large"></i><b class="caret"></b></a>
                        <div class="dropdown-menu" style="padding: 5px;">
                        {{ Form::open(array('url' => 'signin')) }}
                        <fieldset>
                            <div class="form-group" style="margin:5px">
                                {{ Form::label('username', Lang::get('keepaneyeon.username')) }}
                                {{ Form::text('username', null, array("class" => "form-control input-small", "placeholder" => Lang::get('keepaneyeon.ph-username'))) }}
                            </div>
                            <div class="form-group" style="margin:5px">
                                {{ Form::label('password', Lang::get('keepaneyeon.password')) }}
                                {{ Form::password('password', array("class" => "form-control input-small", "placeholder" => Lang::get('keepaneyeon.ph-password'))) }}
                            </div>
                        {{ Form::submit(Lang::get('keepaneyeon.signin'), array("class" => "col-lg-6 pull-right btn btn-default btn-small")) }}
                        <a href="{{ URL::route('SignUp') }}" class="col-lg-6 pull-right btn btn-default btn-small">{{ Lang::get('keepaneyeon.signup') }}</a>
                        </fieldset>
                        {{ Form::close() }}
                        </div>
                    </li>
                        {{ Form::open(array('url' => 'signin', 'class' => 'navbar-form visible-sm')) }}
                        <fieldset>
                            <div class="form-group" style="margin:5px">
                                {{ Form::label('username', Lang::get('keepaneyeon.username')) }}
                                {{ Form::text('username', null, array("class" => "form-control input-small", "placeholder" => Lang::get('keepaneyeon.ph-username'))) }}
                            </div>
                            <div class="form-group" style="margin:5px">
                                {{ Form::label('password', Lang::get('keepaneyeon.password')) }}
                                {{ Form::password('password', array("class" => "form-control input-small", "placeholder" => Lang::get('keepaneyeon.ph-password'))) }}
                            </div>
                        {{ Form::submit(Lang::get('keepaneyeon.signin'), array("class" => "col-lg-6 pull-right btn btn-default btn-small")) }}
                        <a href="{{ URL::route('SignUp') }}" class="col-lg-6 pull-right btn btn-default btn-small">{{ Lang::get('keepaneyeon.signup') }}</a>
                        </fieldset>
                        {{ Form::close() }}
                    @endif
                </li>
            </ul>
        </div>
    </div>
</div> 
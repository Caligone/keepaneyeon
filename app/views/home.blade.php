@extends('main_template')

@section('content')
<div class="jumbotron">
    <h1><i class="icon-eye-open" style="font-size:3em;"></i></h1>
    <p class="lead">{{ Lang::get('keepaneyeon.home-lead') }}</p>
    <p>{{Lang::get('keepaneyeon.home-content')}}</p>
</div>
<hr/>
<div class="container col-lg-6 col-lg-offset-3 panel tweet-box">
	<h3 class="text-center"><span class="icon-stack"><i class="icon-check-empty icon-stack-base"></i><i class="icon-twitter"></i></span> Twitter</h3>
	<div id="tweets">
		<i class="icon-spinner icon-spin icon-large"></i> {{ Lang::get('keepaneyeon.tweets-loading') }}
	</div>
</div>
@stop

@section('scripts')
	{{ HTML::script('assets/js/twitter.js') }}
@stop
<?php

class RouteTest extends TestCase {
	
	public function testMainRoute()
	{
		$crawler = $this->client->request('GET', '/');
		$this->assertTrue($this->client->getResponse()->isOk());

		$crawler = $this->client->request('GET', '/about');
		$this->assertTrue($this->client->getResponse()->isOk());

		$crawler = $this->client->request('GET', '/contact');
		$this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testSignIn()
	{
		if(Auth::check())
			Auth::logout();

		$crawler = $this->client->request('POST', '/signin', array('username' => 'admin', 'password' => 'admin'));
		$this->assertTrue($this->client->getResponse()->isOk());
		$this->assertRedirectedToRoute('/');
		//$this->assertCount(1, $crawler->filter('h1:contains("Hello World!")'));

	}

}
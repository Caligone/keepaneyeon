<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	App::setLocale(Session::get('locale', Config::get('app.locale')));
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

# Need to be logged in
Route::filter('auth', function()
{
	if (Auth::guest())
	{
        Session::flash('danger', "<strong>You need to be logged in.</strong> Our eyes are more open than you can imagin...");
		return Redirect::route('Home');
	}
});


/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

# Need to be logged out
Route::filter('guest', function()
{
	if (Auth::check())
	{
        Session::flash('danger', "<strong>You already have an account.</strong> Our eyes are more open than you can imagin...");
		return Redirect::route('Home');
	}
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
        Session::flash('danger', "<strong>Error or hacking tentative ?</strong> Our eyes are more open than you can imagin...");
		throw new Illuminate\Session\TokenMismatchException;
	}
});

<?php 

###
# English translation file of KeepAnEyeOn app
# Global standards :
# No space
###
return array(

	#########
	# Forms #
	#########
	# Standards : [c|ph]-[Name]
	# ph = placeholder
	# c = confirm
	####
		# Labels
		'old-password'	=> 'Mot de passe actuel',
		'new-password'	=> 'Nouveau mot de passe',
		'c-new-password'	=> 'Confirmation du nouveau mot de passe',
		'email'	=> 'Adresse email',
		'c-email'	=> 'Confirmation de l\'adresse email',
		'username'	=> 'Nom du compte',
		'password'	=> 'Mot de passe',
    	'c-password'	=> 'Confirmation du mot de passe',
    	'url'	=> 'URL',

		# Placeholders
		'ph-old-password'	=> 'Entrez votre mot de passe pour toute modification',
		'ph-new-password'	=> 'Entrez votre mot de passe',
		'ph-c-new-password'	=> 'Confirmez votre mot de passe',
		'ph-email'	=> 'Entrez votre email',
		'ph-c-email'	=> 'Confirmez votre email',
		'ph-username'	=> 'Entrez votre nom de compte',
		'ph-password'	=> 'Entrez votre mot de passe',
		'ph-c-password'	=> 'Confirmez votre mot de passe',
		'ph-c-email'	=> 'Confirmez votre email',
    	'ph-url'	=> 'Entrez l\'URL',

		# Buttons
		'reset'	=> 'Réinitialiser',
		'edit'	=> 'Editer',
		'signin'     => 'Utiliser',
		'signup'     => 'Créer',
    	'signout'     => 'Quitter',
    	'add'     => 'Ajouter',

	##########
	# Alerts #
	##########
	# Standards : [s|i|d]-[Name]-[Reason]
	# s = success
	# i = info
	# d = danger
	####
		# Success
		's-signin'     => '<strong>Vous êtes désormais connecté !</strong> Gérez vos yeux avec soin.',
		's-signup'     => '<strong>Votre compte a été créé avec succès !</strong> Vérifiez vos emails pour l\'activer et commencez à déposer vos yeux sur la toile.',
		's-signout'     => '<strong>Vous êtes maintenant déconnecté !</strong> Nous garderons vos yeux jusqu\'à votre retour.',
		's-activation'     => '<strong>Votre compte est désormais activé !</strong> Vous pouvez gérer vos yeux comme vous le souhaitez.',
		's-resend'     => '<strong>L\'email d\'activation a été envoyé à nouveau.</strong> Vérifiez vos emails et commencez à déposer vos yeux sur la toile.',
		's-retrieve'     => '<strong>Un email de récupération de mot de passe vous a été envoyé.</strong> Vérifiez vos emails pour changer votre mot de passe.',
		's-retrievemail'     => '<strong>Votre mot de passe a été changé et vous a été envoyé par email.</strong> Vous pouvez désormais gérer vos yeux.',
		's-account-email'     => '<strongVotre compte a bien été édité.</strong> Votre adresse email a été changée.',
		's-account-password'     => '<strong>Votre compte a bien été édité.</strong> Votre mot de passe a été changé.',
		's-account'   => '<strong>Votre compte a bien été édité.</strong> Votre adresse email et votre mot de passe ont été changés.',
		's-account-nothing'   => '<strong>Votre compte n\'a pas été édité.</strong> Rien n\'a changé.',
		's-locale-change'	=> '<strong>Bien !</strong> Nous parlerons donc français.',
        's-activate'    => '<strong>Votre oeil a été ouvert avec succès.</strong> Il regarde actuellement <a href=":url" class="alert-link">:url</a>.',
		
		# Info
		// Empty
	
		# Danger
		'f-signin-activation' => '<strong>Vous devez activer votre compte avant de l\'utiliser !</strong> <a href="'.URL::route('Resend', array('token' => ':token')).'" class="alert-link">Renvoi du lien d\'activation</a>.',
		'f-signin-password'     => '<strong>Votre mot de passe est incorrect !</strong> Ouvrez les yeux et réessayez ou bien <a href="'.URL::route('Retrieve', ':id').'" class="alert-link">retrouvez votre compte</a>.',
		'f-signin-username'     => '<strong>Votre identifiant est incorrect !</strong> Ouvrez les yeux et réessayez.',
		'f-activation'     => '<strong>Votre jeton est invalide.</strong> Nos yeux sont plus ouverts que vous ne le pensez...',
		'f-resend'     => '<strong>Votre jeton est invalide.</strong> Nos yeux sont plus ouverts que vous ne le pensez...',
		'f-retrieve'     => '<strong>votre identifiant est invalide.</strong> Nos yeux sont plus ouverts que vous ne le pensez...',
		'f-retrievemail'     => '<strong>Votre jeton est invalide.</strong> Nos yeux sont plus ouverts que vous ne le pensez...',
		'f-account-actualpassword'     => '<strong>Votre mot de passe actuel est incorrect !</strong>',
        'f-eyecredit'   => '<strong>Vous n\'avez pas assez de crédit pour ouvrir un nouvel oeil.</strong> Vous pouvez en obtenir <a href="'.URL::route('EyeCredit').'" class="alert-link">ici</a>.',
        'f-already'	=> '<strong>Vous avez déjà un oeil pointé vers ce site !</strong> Vous ne pouvez pas avoir plusieurs yeux sur la même url.',

	##########
	# Others #
	##########
		'icon-locale'	=> '<i class="icon-flag-alt"></i> <span class="lang-icon">EN/<b>FR</b></span>',
		'home-lead'	=>	'Vous voulez utiliser <button type="button" class="btn btn-default" style="padding:5px 10px">KeepAnEyeOn</button> pour surveiller un élément de la toile ?',
		'signup-lead'	=> 'Création de compte',
		'account-lead'	=> 'Compte de :username',
        'tweets-loading'	=> 'Chargement des tweets...',
        'manager-lead'	=>	'Gérez vos yeux',
        'creation-date' => 'Date de création',
        'update-date' => 'Date de modification',
        'home-content'  => 'KeepAnEyeOn propose de garder un oeil sur vos sites Internet favoris et vous notifier en cas de changements en approchant au mieux le temps réel. Grâce à KeepAnEyeOn, soyez le premier averti d\'une promotion ou d\'une publication spéciale.',
	
    ########
	# Menu #
	########
		# Main
		'home'     => 'Accueil',
		'about'     => 'A propos',
		'contact'     => 'Contact',

		# User
		'account'     => 'Compte',
		'get-eyes'     => 'Obtenir des yeux',
		'add-eye'     => 'Activer des yeux',
		'del-eye'     => 'Désactiver des yeux',
		'edit-eye'     => 'Editer des yeux',
		'manage-eyes'	=> 'Gérer ses yeux',
        'eyes'    => 'Yeux',
        'eye'	=> 'Oeil',

);
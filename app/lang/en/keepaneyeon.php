<?php 

###
# English translation file of KeepAnEyeOn app
# Global standards :
# No space
###
return array(

	#########
	# Forms #
	#########
	# Standards : [c|ph]-[Name]
	# ph = placeholder
	# c = confirm
	####
		# Labels
		'old-password'	=> 'Old password',
		'new-password'	=> 'New password',
		'c-new-password'	=> 'Confirm password',
		'email'	=> 'Email',
		'c-email'	=> 'Confirm email',
		'username'	=> 'Username',
		'password'	=> 'Password',
		'c-password'	=> 'Confirm password',
    	'url'	=> 'URL',

		# Placeholders
		'ph-old-password'	=> 'Enter password for any modification',
		'ph-new-password'	=> 'Enter password',
		'ph-c-new-password'	=> 'Confirm password',
		'ph-email'	=> 'Enter email',
		'ph-c-email'	=> 'Confirm email',
		'ph-username'	=> 'Enter username',
		'ph-password'	=> 'Enter password',
		'ph-c-password'	=> 'Confirmez password',
		'ph-c-email'	=> 'Confirm email',
    	'ph-url'	=> 'Enter URL',

		# Buttons
		'reset'	=> 'Reset',
		'edit'	=> 'Edit',
		'signin'     => 'Sign In',
		'signup'     => 'Sign Up',
    	'signout'     => 'Sign Out',
    	'add'     => 'Add',

	##########
	# Alerts #
	##########
	# Standards : [s|i|d]-[Name]-[Reason]
	# s = success
	# i = info
	# d = danger
	####
		# Success
		's-signin'     => '<strong>You are now logged in !</strong> Manage your eyes carefully.',
		's-signup'     => '<strong>Your account is now created.</strong> Check your email to activate it and put your eyes on the web.',
		's-signout'     => '<strong>You are now logged out !</strong> We will keep your eyes open until you come back.',
		's-activation'     => '<strong>Your account is now activated.</strong> You can manage your eyes.',
		's-resend'     => '<strong>The activation email sent again.</strong> Check your email to activate it and put your eyes on the web.',
		's-retrieve'     => '<strong>The retrieve email sent.</strong> Check your email to change your password and put your eyes on the web.',
		's-retrievemail'     => '<strong>Your password is changed and sent by email.</strong> You can manage your eyes.',
		's-account-email'     => '<strong>Your account has been edited.</strong> Your email has been changed.',
		's-account-password'     => '<strong>Your account has been edited.</strong> Your password has been changed.',
		's-account'   => '<strong>Your account has been edited.</strong> Your email and your password have been changed.',
		's-account-nothing'   => '<strong>Your account has not been edited.</strong> Nothing changed.',
    	's-locale-change'	=> 'We will speak english now.',
    	's-activate'	=> '<strong>Your eye has been open with success.</strong> It is watching <a href=":url" class="alert-link">:url</a>.',
		
		# Info
		// Empty
	
		# Danger
		'f-signin-activation' => '<strong>You need to activate your account before use it !</strong> <a href="'.URL::route('Resend', array('token' => ':token')).'" class="alert-link">Resend the activation mail</a>.',
		'f-signin-password'     => '<strong>Your password is wrong !</strong> Open your eyes and try again or <a href="'.URL::route('Retrieve', ':id').'" class="alert-link">retrieve your ID</a>.',
		'f-signin-username'     => '<strong>Your username is wrong !</strong> Open your eyes and try again.',
		'f-activation'     => '<strong>Your token is invalid.</strong> Our eyes are more open than you can imagin...',
		'f-resend'     => '<strong>Your token is invalid.</strong> Our eyes are more open than you can imagin...',
		'f-retrieve'     => '<strong>Your id is invalid.</strong> Our eyes are more open than you can imagin...',
		'f-retrievemail'     => '<strong>Your token is invalid.</strong> Our eyes are more open than you can imagin...',
		'f-account-actualpassword'     => '<strong>Your actual password is incorrect !</strong>',
        'f-eyecredit'   => '<strong>You have not enought credit to open another eye.</strong> You can get credits <a href="'.URL::route('EyeCredit').'" class="alert-link">here</a>.',
        'f-already'	=> '<strong>You already have an eye on this website.</strong> You can\'t have several eyes looking the same url.',

	##########
	# Others #
	##########
		'icon-locale'	=> '<i class="icon-flag"></i> <span class="lang-icon "><b>EN</b>/FR</span>',
		'home-lead'	=>	'You want to  <button type="button" class="btn btn-default" style="padding:5px 10px">KeepAnEyeOn</button> something on the Web?',
		'signup-lead'	=> 'Sign Up',
    	'account-lead'	=> ':username\'s Account',
    	'tweets-loading'	=> 'Loading tweets...',
        'manager-lead'	=>	'Manage your eyes',
        'creation-date' => 'Creation date',
        'update-date' => 'Update date',
        'home-content'  => 'KeepAnEyeOn allows you to keep an eye on your favourite websites. You can be warned when there is a change on it, almost in real time. With KeepAnEyeOn, be the first to be aware of a sale, or any special publication.',
        
	########
	# Menu #
	########
		# Main
		'home'     => 'Home',
		'about'     => 'About',
		'contact'     => 'Contact',

		# User
		'account'     => 'Account',
		'get-eyes'     => 'Get eyes',
		'add-eye'     => 'Add eye',
		'del-eye'     => 'Remove eye',
		'edit-eye'     => 'Edit eye',
    	'manage-eyes'	=> 'Manage eyes',
        'eyes'	=> 'Eyes',
        'eye'	=> 'Eye',

);
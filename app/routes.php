<?php

# # # # # # #
# Everyone  #
# # # # # # #
Route::group(array(), function()
{
    Route::get('/', array('as' => 'Home', 'uses' => 'HomeController@home'));

	Route::get('/about', array('as' => 'About', 'uses' => 'HomeController@about'));

	Route::get('/contact', array('as' => 'Contact', 'uses' => 'HomeController@contact'));

	Route::get('/lang', array('as' => 'LangAction', 'uses' => 'HomeController@langAction'));
});


# # # # #
# Guest #
# # # # # 
Route::group(array('before' => 'guest'), function()
{
	# Connection (Form + Action)
	Route::get('/signin', array('as' => 'SignIn', 'uses' => 'UserController@signIn'));
	Route::post('/signin', array('as' => 'SignInAction', 'before' => 'csrf', 'uses' => 'UserController@signInAction'));

	# Registration (Form + Action)
	Route::get('/signup', array('as' => 'SignUp', 'uses' => 'UserController@signUp'));
	Route::post('/signup', array('as' => 'SignUpAction', 'before' => 'csrf', 'uses' => 'UserController@signUpAction'));

	# Validate (Action)
	Route::get('/activate/{token}', array('as' => 'Activate', 'uses' => 'UserController@activateAction'));

	# Resend (Action)
	Route::get('/resend/{token}', array('as' => 'Resend', 'uses' => 'UserController@resendAction'));

	# Retrieve (Action)
	Route::get('/retrieve/{id}', array('as' => 'Retrieve', 'uses' => 'UserController@retrieveAction'));
	Route::get('/retrieve-mail/{token}', array('as' => 'RetrieveMail', 'uses' => 'UserController@retrieveMailAction'));
});


# # # # #
# Auth  #
# # # # # 
Route::group(array('before' => 'auth'), function()
{
	# Deconnection (Action)
	Route::get('/signout', array('as' => 'SignOutAction', 'uses' => 'UserController@signOutAction'));

	# Account
	Route::get('/account', array('as' => 'Account', 'uses' => 'UserController@account'));
	Route::post('/account', array('as' => 'AccountAction', 'before' => 'csrf', 'uses' => 'UserController@accountAction'));
    
    # Manage Eyes
    Route::get('/eye/manage', array('as' => 'ManageEyes', 'uses' => 'EyeController@manager'));
    Route::get('/eye/edit/{id}', array('as' => 'EditEye', 'uses' => 'EyeController@edit'));
    Route::get('/eye/remove/{id}', array('as' => 'RemoveEye', 'uses' => 'EyeController@remove'));

    Route::get('/eye/activate', array('as' => 'ActivateEye', 'uses' => 'EyeController@activate'));
	Route::post('/eye/activate', array('as' => 'ActivateEyeAction', 'before' => 'csrf', 'uses' => 'EyeController@activateAction'));
    
    # Get Eye
    Route::get('/eye/credit', array('as' => 'EyeCredit', 'uses' => 'EyeController@eyecredit'));
});

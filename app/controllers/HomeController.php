<?php

class HomeController extends BaseController {

	public function home()
	{
		return View::make('home');
	}

	public function about()
	{
		return View::make('about');
	}

	public function contact()
	{
		return View::make('contact');
	}

	public function langAction()
	{
		if(Session::get('locale') == 'en' || !Session::has('locale'))
		{
			Session::put('locale', 'fr');
			App::setLocale('fr');
		}
		else
		{
			Session::put('locale', 'en');
			App::setLocale('en');
		}
		Session::flash('success', Lang::get('keepaneyeon.s-locale-change'));
		return Redirect::back();
	}
}
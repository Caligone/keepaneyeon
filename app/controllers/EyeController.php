<?php

class EyeController extends BaseController {

	/**
	 * Display the manage page
	 * GET Method
	 * @return View manage page
	 */
	public function manager()
	{
		$currentUser = Auth::user();
		$eyes = $currentUser->eyes;

		return View::make('eye.manager', array('eyes' => $eyes));
	}


	/**
	 * Display the activate Form
	 * GET Method
	 * @return View activate form
	 */
	public function activate()
	{
		return View::make('eye.activate');
	}

	/**
	 * Activate Action
	 * POST Method
	 * @return View Home with danger alert if account not active or wrong combinaison, success alert else
	 */
	public function activateAction()
	{
		$rules = array(
    		'url' => 'required|url'
		);
		$validator = Validator::make(Input::all(), $rules);
	 
	 	// Si le formulaire est mal rempli
		if ($validator->fails())
			return Redirect::to('eye/activate')->withErrors($validator)->withInput();
		else
		{
			// Si l'utilisateur n'a plus de crédit
            if(Auth::user()->eyecredit < 1)
            {
        		Session::flash('danger', Lang::get('keepaneyeon.f-eyecredit'));
    			return Redirect::route('Home');
            }
            else
            {
            	$creatingEye = Eye::where('url', '=', Input::get('url'))->get()->first();
            	// Si l'oeil existe déjà
            	if($creatingEye instanceof Eye)
            	{
            		$idAddedEye = $creatingEye->id;
            		// Si l'utilisateur à déjà ouvert un oeil sur cette url
            		foreach ($creatingEye->users as $user)
            		{
            			if($user->id == Auth::user()->id)
            			{
			    			Session::flash('danger', Lang::get('keepaneyeon.f-already'));
			    			return Redirect::route('Home');
            			}
            		}
            	}
            	else
            	{
	    			$creatingEye = new Eye;
	    			$creatingEye->url = Input::get('url');
	    			$creatingEye->save();
	    			$idAddedEye = $creatingEye->id;
	           	}

                $currentUser = Auth::user();

	           	$creatingEye->users()->attach($currentUser->id);
	           	$creatingEye->save();
                $currentUser->eyecredit--;
                $currentUser->save();
        		Session::flash('success', Lang::get('keepaneyeon.s-activate', array('url' => $creatingEye->url)));
                
            }
		}
    			return Redirect::route('Home');
	}
    

	public function eyecredit()
	{
        $user = Auth::user();
        $user->eyecredit++;
        $user->save();
    	return Redirect::route('Home');
	}
}
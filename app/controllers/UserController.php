<?php

class UserController extends BaseController {

	/**
	 * Display the connection Form
	 * GET Method
	 * @return View connection form
	 */
	public function signIn()
	{
		return View::make('user.signin');
	}

	/**
	 * Connection Action
	 * POST Method
	 * @return View Home with danger alert if account not active or wrong combinaison, success alert else
	 */
	public function signInAction()
	{
		$username = Input::get('username');
		$password = Input::get('password');
		 
		if(Auth::attempt(array('username' => $username, 'password' => $password)))
		{
			if(Auth::user()->active == 'no')
			{
				$token = Auth::user()->token;
				Auth::logout();
				Session::flash('danger', Lang::get('keepaneyeon.f-signin-activation', array('token' => $token)));
			}
			else
			{
				Session::flash('success', Lang::get('keepaneyeon.s-signin'));
			}
		}
		else
		{
			$user = User::where('username', '=', $username)->get()->first();
			if($user instanceof User)
			{
				Session::flash('danger', Lang::get('keepaneyeon.f-signin-password', array('id' => $user->id)));
			}
			else
			{
				Session::flash('danger', Lang::get('keepaneyeon.f-signin-username'));
			}
		}
		return Redirect::back();
	}
	
	/**
	 * Handle the logout button in the menu bar
	 * GET Method
	 * @return View Home with danger alert if not logged, success alert else
	 */
	public function signOutAction()
	{
		Auth::logout();
		Session::flash('success', Lang::get('keepaneyeon.s-signout'));
		return Redirect::route('Home');
	}
	
	/**
	 * Display the register form
	 * GET Method
	 * @return View Register form
	 */
	public function signUp()
	{
		return View::make('user.signup');
	}

	/**
	 * Handle the register form
	 * POST Method
	 * @return View Register form
	 */
	public function signUpAction()
	{
		$rules = array(
			'username' => 'required|unique:users|min:5|max:20|alpha_num',
			'password' => 'required|min:6|max:20|alpha_dash',
			'confirm-password' => 'required|same:password',
			'email' => 'required|unique:users|min:6|max:35|email',
			'confirm-email' => 'required|same:email'
		);
		$validator = Validator::make(Input::all(), $rules);
	 
		if ($validator->fails())
			return Redirect::to('signup')->withErrors($validator)->withInput();
		else
		{
			$creatingUser = new User;
			$creatingUser->username = Input::get('username');
			$creatingUser->email = Input::get('email');
			$creatingUser->password = Hash::make(Input::get('password'));
			$creatingUser->generateToken();
			$creatingUser->save();
			$mailParams = array('user' => $creatingUser);
			Mail::queue('emails.welcome', $mailParams, function($message) use ($creatingUser)
			{
			    $message->to($creatingUser->email, $creatingUser->username)->subject('Activation of your eyes');
			});
			Session::flash('success', Lang::get('keepaneyeon.s-signup'));
			return Redirect::route('Home');
		}
	}

	/**
	 * Handle the validation link
	 * GET Method
	 * @param  String $token Security token
	 * @return View Home with danger alert if not logged, success alert else
	 */
	public function activateAction($token)
	{
		$user = User::where('token', '=', $token)->get()->first();
		if($user instanceof User)
		{
			$user->active = 'yes';
			$user->token = null;
			$user->save();
			Auth::login($user);
			Session::flash('success', Lang::get('keepaneyeon.s-activation'));
		}
		else
		{
			Session::flash('danger', Lang::get('keepaneyeon.f-activation'));
		}
		return Redirect::route('Home');
	}

	/**
	 * Handle the resend validation mail
	 * GET Method
	 * @param  String $token Security token
	 * @return View Home with danger alert if not logged, success alert else
	 */
	public function resendAction($token)
	{
		$user = User::where('token', '=', $token)->get()->first();
		if($user instanceof User)
		{
			$mailParams = array('user' => $user);
			Mail::queue('emails.welcome', $mailParams, function($message) use ($user)
			{
			    $message->to($user->email, $user->username)->subject('Activation of your eyes');
			});
			Session::flash('success', Lang::get('keepaneyeon.s-resend'));
		}
		else
		{
			Session::flash('danger', Lang::get('keepaneyeon.f-resend'));
		}
		return Redirect::route('Home');
	}

	/**
	 * Handle the retrieve password link
	 * GET Method
	 * @param  integer $id ID of the user
	 * @return View Home with danger alert if not logged, success alert else
	 */
	public function retrieveAction($id)
	{
		$user = User::find($id);
		if($user instanceof User)
		{
			$user->generateToken();
			$user->save();
			$mailParams = array('user' => $user);
			Mail::queue('emails.retrieve', $mailParams, function($message) use ($user)
			{
			    $message->to($user->email, $user->username)->subject('Retrieve your eyes');
			});
			Session::flash('success', Lang::get('keepaneyeon.s-retrieve'));
		}
		else
		{
			Session::flash('danger', Lang::get('keepaneyeon.f-retrieve'));
		}
		return Redirect::route('Home');
	}

	/**
	 * Handle the retrieve password mail
	 * GET Method
	 * @param  String $token Security token
	 * @return View Home with danger alert if not logged, success alert else
	 */
	public function retrieveMailAction($token)
	{
		$user = User::where('token', '=', $token)->get()->first();
		if($user instanceof User)
		{
			$newPassword = $user->generatePassword();
			$user->token = null;
			$user->save();
			$mailParams = array('user' => $user, 'newPassword' => $newPassword);
			Mail::queue('emails.password', $mailParams, function($message) use ($user)
			{
			    $message->to($user->email, $user->username)->subject('Retrieve your eyes');
			});
			Session::flash('success', Lang::get('keepaneyeon.s-retrievemail'));
		}
		else
		{
			Session::flash('danger', Lang::get('keepaneyeon.f-retrievemail'));
		}
		return Redirect::route('Home');
	}
	
	/**
	 * Display the account form
	 * GET Method
	 * @return View Account form
	 */
	public function account()
	{
		return View::make('user.account', array('user' => Auth::user()));
	}
	
	/**
	 * Handle the account form
	 * POST Method
	 * @return View Home with danger alert if not logged, success alert else
	 */
	public function accountAction()
	{
		$rules = array(
			'password' => 'required|different:new-password',
			'new-password' => 'min:6|max:20|alpha_dash',
			'confirm-new-password' => 'required_with:new-password|same:new-password',
			'email' => 'unique:users|min:6|max:35|email',
			'confirm-email' => 'required_with:email|same:email'
		);

		$incorrectPassword = false;

		$messages = array();
		if(!Hash::check(Input::get('password'), Auth::user()->password))
		{
			Session::flash('danger', Lang::get('keepaneyeon.f-account-actualpassword'));
			$incorrectPassword = true;
		}


		$validator = Validator::make(Input::all(), $rules, $messages);
	 
		if ($validator->fails() || $incorrectPassword)
		{
			return Redirect::to('account')->withErrors($validator)->withInput();
		}
		else
		{
			$passwordEdition = false;
			if(strlen(Input::get('new-password')) > 5)
			{
				$user = Auth::user();
				$user->password = Hash::make(Input::get('new-password'));
				$user->save();
				$passwordEdition = true;
			}

			$emailEdition = false;
			if(strlen(Input::get('email')) > 5)
			{
				$user = Auth::user();
				$user->email = Input::get('email');
				$user->save();
				$emailEdition = true;
			}
			if($emailEdition || $passwordEdition)
			{
				if($emailEdition && $passwordEdition)
					Session::flash('success', Lang::get('keepaneyeon.s-account'));
				elseif($emailEdition)
					Session::flash('success', Lang::get('keepaneyeon.s-account-email'));
				else
					Session::flash('success', Lang::get('keepaneyeon.s-account-username'));
			}
			else
			{
				Session::flash('success', Lang::get('keepaneyeon.s-account-nothing'));
			}
			return Redirect::route('Home');

		}
	}
}
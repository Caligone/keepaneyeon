<?php

use Illuminate\Database\Migrations\Migration;

class CreateKeepaneyeon extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// User Table
        Schema::create('users', function($table) {
            $table->increments('id')->unsigned();
            $table->string('username', 64)->unique();
            $table->string('password', 64);
            $table->string('email', 64)->unique();
            $table->integer('eyecredit')->unsigned()->default(0);
            $table->string('token', 64)->nullable()->default(null);
            $table->enum('active', array('yes', 'no'))->default('no');
            $table->enum('statut', array('user', 'admin'))->default('user');
            $table->timestamps();
        });
        
        // Order Table
        Schema::create('orders', function($table) {
            $table->increments('id')->unsigned();
            $table->string('label', 128);
            $table->float('amount');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
        
        // Eye Table
        Schema::create('eyes', function($table) {
            $table->increments('id')->unsigned();
            $table->string('url')->unique();
            $table->timestamps();
        });
        
        // Eye_User Table
        Schema::create('eye_user', function($table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('eye_id')->unsigned();
            $table->foreign('eye_id')->references('id')->on('eyes')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
        
        // Giftcode Table
        Schema::create('giftcodes', function($table) {
            $table->increments('id')->unsigned();
            $table->string('code', 32);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
	}
            

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    	Schema::drop('orders');
    	Schema::drop('eye_user');
    	Schema::drop('giftcodes');
    	Schema::drop('eyes');
        Schema::drop('users');
	}

}
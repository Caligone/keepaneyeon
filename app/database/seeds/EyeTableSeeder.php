<?php
class EyeTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('eyes')->insert(
 
            array(
                array(
                    'id' => 1,
                    'url' => 'http://google.fr',
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                ),
 
                array(
                    'id' => 2,
                    'url' => 'http://caligone.fr',
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                )
            )
        );
    }
}
<?php
class UserTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('users')->insert(
 
            array(
                array(
                    'id' => 1,
                    'username' => 'admin',
                    'password' => Hash::make('admin'),
                    'email' => 'admin@test.fr',
                    'statut' => 'admin',
                    'eyecredit' => 0,
                    'active' => 'yes',
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                ),
 
                array(
                    'id' => 2,
                    'username' => 'demo',
                    'password' => Hash::make('demo'),
                    'email' => 'demo@test.fr',
                    'statut' => 'user',
                    'eyecredit' => 0,
                    'active' => 'no',
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                ),
 
                array(
                    'id' => 3,
                    'username' => 'demobis',
                    'password' => Hash::make('demobis'),
                    'email' => 'demobis@test.fr',
                    'statut' => 'user',
                    'eyecredit' => 5,
                    'active' => 'yes',
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                )
            )
        );
    }
}

<?php
class GiftcodeTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('giftcodes')->insert(
 
            array(
                array(
                    'id' => 1,
                    'user_id' => null,
                    'code' => 'AMAZING',
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                ),
 
                array(
                    'id' => 2,
                    'user_id' => 3,
                    'code' =>'AWESOME',
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                )
            )
        );
    }
}
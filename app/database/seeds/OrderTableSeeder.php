<?php
class OrderTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('orders')->insert(
 
            array(
                array(
                    'id' => 1,
                    'label' => '3 Eyes (Paypal)',
                    'amount' => 1.5,
                    'user_id' => 3,
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                ),
 
                array(
                    'id' => 2,
                    'label' => '2 Eyes (Giftcode)',
                    'amount' => 0,
                    'user_id' => 3,
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                )
            )
        );
    }
}

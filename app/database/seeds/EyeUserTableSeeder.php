<?php
class EyeUserTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('eye_user')->insert(
 
            array(
                array(
                    'id' => 1,
                    'user_id' => 1,
                    'eye_id' => 1,
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                ),
 
                array(
                    'id' => 2,
                    'user_id' => 1,
                    'eye_id' => 2,
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                )
            )
        );
    }
}
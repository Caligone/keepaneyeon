hashtag_regexp = /#([a-zA-Z0-9]+)/g;
mention_regexp = /@([a-zA-Z0-9_]+)/g;

function linkHashtags(text) {
    return text.replace(
        hashtag_regexp,
        '<a class="hashtag" href="http://twitter.com/#search?q=$1">#$1</a>'
    );
} 
function linkMention(text) {
    return text.replace(
        mention_regexp,
        '<a class="hashtag" href="http://twitter.com/$1">@$1</a>'
    );
}

$.ajax({
url: "assets/ajax/twitter.php",
type: "POST",
dataType : "json",
data: { grant_type: "client_credentials" }
}).done(function(data) {
    var tweetList = "";
    $(data).each(function(index, value)
    {
        tweetList += '<div class="tweet">';
        
        tweetList += ' '+linkMention(linkHashtags(value.text));
        tweetList += '</div>';
    });
    $("#tweets").html(tweetList);
});